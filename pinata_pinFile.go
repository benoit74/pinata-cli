package pinatacli

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"
	"time"

	log "github.com/sirupsen/logrus"
)

type pinFileResult struct {
	IpfsHash  string    `json:"IpfsHash"`
	PinSize   int       `json:"PinSize"`
	Timestamp time.Time `json:"Timestamp"`
}

type PinFileCriteria struct {
	File              string
	CIDVersion        string
	WrapWithDirectory bool
	Name              string
	KeyValues         map[string]string
	ReplicationInfos  []PinReplicationInfo
}

type PinFileResult struct {
	IpfsHash  string
	PinSize   int
	Timestamp time.Time
}

func (result pinFileResult) String() string {
	return fmt.Sprintf("IpfsHash: %s | PinSize: %d | Timestamp: %s",
		result.IpfsHash, result.PinSize, result.Timestamp)
}

func (result PinFileResult) String() string {
	return fmt.Sprintf("IpfsHash: %s | PinSize: %d | Timestamp: %s",
		result.IpfsHash, result.PinSize, result.Timestamp)
}

func (pinata Pinata) writeAllContent(input string, options *pinataOptions, metadata *pinataMetadata) (*io.PipeReader, *string, []error) {

	pinataOptionsBytes, err := json.Marshal(options)
	pinataOptionsStr := string(pinataOptionsBytes)

	pinataMetadataBytes, err := json.Marshal(metadata)
	pinataMetadataStr := string(pinataMetadataBytes)

	stats, err := os.Stat(input)
	if os.IsNotExist(err) {
		return nil, nil, []error{fmt.Errorf("File or directory is missing: %s", input)}
	}

	var files []string
	fileIsASingleFile := !stats.IsDir()
	if fileIsASingleFile {
		files = append(files, stats.Name())
	} else {
		err = filepath.Walk(input,
			func(path string, info os.FileInfo, err error) error {
				if err != nil {
					return err
				}
				if !info.IsDir() {
					relPath, _ := filepath.Rel(input, path)
					files = append(files, relPath)
				}
				return nil
			})

		if err != nil {
			return nil, nil, []error{fmt.Errorf("Fatal error while exploring directory '%s'", input), err}
		}
	}

	r, w := io.Pipe()
	m := multipart.NewWriter(w)
	go func() {
		defer w.Close()
		defer m.Close()

		for _, f := range files {
			var part io.Writer
			var err error
			if fileIsASingleFile {
				part, err = m.CreateFormFile("file", f)
			} else {
				part, err = m.CreateFormFile("file", filepath.Join(stats.Name(), f))
			}
			if err != nil {
				return
			}
			var file *os.File
			if fileIsASingleFile {
				file, err = os.Open(input)
			} else {
				file, err = os.Open(filepath.Join(input, f))
			}
			if err != nil {
				return
			}
			defer file.Close()
			if _, err = io.Copy(part, file); err != nil {
				return
			}
		}

		if options != nil {
			err = m.WriteField("pinataOptions", pinataOptionsStr)
		}

		if metadata != nil {
			err = m.WriteField("pinataMetadata", pinataMetadataStr)
		}

	}()

	boundary := m.Boundary()
	return r, &boundary, nil

}

func (pinata Pinata) pinFile(ids PinataIdentifiers, criteria PinFileCriteria) (PinFileResult, []error) {

	var result PinFileResult

	unionCriteria := pinCriteriaUnion{
		CIDVersion:        criteria.CIDVersion,
		WrapWithDirectory: &criteria.WrapWithDirectory,
		Name:              criteria.Name,
		KeyValues:         criteria.KeyValues,
		ReplicationInfos:  criteria.ReplicationInfos,
	}

	pinataOptions := pinata.buildPinataOptions(unionCriteria)
	pinataMetadata := pinata.buildPinataMetadata(unionCriteria)

	reader, boundary, errs := pinata.writeAllContent(criteria.File, pinataOptions, pinataMetadata)
	if errs != nil {
		return result, append([]error{fmt.Errorf("Failed to prepare content")}, errs...)
	}

	// Uncomment lines below to debug form content
	// !!! could be huge is files are huge, clumsy if files are binary !!!
	/*
		if _, err := io.Copy(os.Stdout, reader); err != nil {
			log.Fatal(err)
		}

		return result, []error{fmt.Errorf("ABORT")}
	*/

	query := "https://api.pinata.cloud/pinning/pinFileToIPFS"
	log.Debug("POST ", query)

	req, err := http.NewRequest("POST", query, reader)
	if err != nil {
		return result, []error{fmt.Errorf("Failed to prepare request for Pinata server with POST at %s", query), err}
	}

	req.Header.Add("Content-Type", fmt.Sprintf("multipart/form-data; boundary=%s", *boundary))
	req.Header.Add("pinata_api_key", ids.ApiKey)
	req.Header.Add("pinata_secret_api_key", ids.SecretApiKey)

	resp, err := httpClient.Do(req)

	if err != nil {
		return result, []error{fmt.Errorf("Failed to call Pinata server with POST at %s", query), err}
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return result, []error{fmt.Errorf("Failed to read body while calling Pinata server with POST at %s", query), err}
	}

	log.Trace("Status code: ", resp.Status)
	log.Trace("Response body: ", string(body))

	if resp.StatusCode != 200 {
		return result, []error{fmt.Errorf("Error while calling Pinata server with POST at %s", query),
			fmt.Errorf("Status code: %s", resp.Status),
			fmt.Errorf("Response body: %s", string(body)),
		}
	}

	var response pinFileResult
	err = json.Unmarshal(body, &response)
	if err != nil {
		return result, []error{fmt.Errorf("Failed to parse JSON response body while calling Pinata server with POST at %s", query), err}
	}

	log.Trace("Response unmarshalled: ", response.String())

	result = PinFileResult{
		IpfsHash:  response.IpfsHash,
		PinSize:   response.PinSize,
		Timestamp: response.Timestamp,
	}

	log.Trace("Return object: ", result.String())

	return result, nil
}
