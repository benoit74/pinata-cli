package pinatacli

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	log "github.com/sirupsen/logrus"
)

type UnpinCriteria struct {
	HashToRemove string
}

type UnpinResult struct {
	Success bool `json:"success"`
}

func (criteria UnpinCriteria) String() string {
	return fmt.Sprintf("HashToRemove: %s", criteria.HashToRemove)
}

func (result UnpinResult) String() string {
	return fmt.Sprintf("Success: %s",
		strconv.FormatBool(result.Success))
}

func (pinata Pinata) unpin(ids PinataIdentifiers, criteria UnpinCriteria) (UnpinResult, []error) {

	var result = UnpinResult{Success: false}

	query := fmt.Sprintf("https://api.pinata.cloud/pinning/unpin/%s", criteria.HashToRemove)
	log.Debug("DELETE ", query)

	req, err := http.NewRequest("DELETE", query, nil)
	if err != nil {
		return result, []error{fmt.Errorf("Failed to prepare request for Pinata server with DELETE at %s", query), err}
	}

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("pinata_api_key", ids.ApiKey)
	req.Header.Add("pinata_secret_api_key", ids.SecretApiKey)

	resp, err := httpClient.Do(req)

	if err != nil {
		return result, []error{fmt.Errorf("Failed to call Pinata server with DELETE at %s", query), err}
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return result, []error{fmt.Errorf("Failed to read body while calling Pinata server with DELETE at %s", query), err}
	}

	log.Trace("Status code: ", resp.Status)
	log.Trace("Response body: ", string(body))
		
	if resp.StatusCode != 200 {
		return result, nil
	}

	return UnpinResult{Success: true}, nil
}
