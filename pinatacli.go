package pinatacli

import (
	"fmt"
	"time"
)

type PinataCli struct {
}

type PinataIdentifiers struct {
	ApiKey       string
	SecretApiKey string
}

type PinataPin struct {
	ID           string
	IpfsPinHash  string
	Size         int
	UserID       string
	DatePinned   time.Time
	DateUnpinned time.Time
	Metadata     PinataPinMetadata
	Regions      []PinataPinRegion
}

type PinataPinRegion struct {
	RegionID                string
	DesiredReplicationCount int
	CurrentReplicationCount int
}

type PinataPinMetadata struct {
	Name      string
	KeyValues map[string]string
}

type PinataPinJob struct {
	ID          string
	IpfsPinHash string
	DateQueued  time.Time
	Name        string
	Status      string
}

type PinReplicationInfo struct {
	Region           string
	ReplicationCount int
}

func (pin PinataPin) String() string {
	return fmt.Sprintf("ID: %s | IpfsPinHash: %s", pin.ID, pin.IpfsPinHash)
}

func (pinataCli *PinataCli) Noop() error {
	return nil
}

func (pinataCli *PinataCli) GetPinsList(ids PinataIdentifiers, criteria ListPinsCriteria) ([]PinataPin, []error) {
	var pinata Pinata
	pins, errs := pinata.getPinsList(ids, criteria)
	if errs != nil {
		return nil, errs
	}
	return pins, nil
}

func (pinataCli *PinataCli) GetPinJobsList(ids PinataIdentifiers, criteria ListPinJobsCriteria) ([]PinataPinJob, []error) {
	var pinata Pinata
	pinJobs, errs := pinata.getPinJobsList(ids, criteria)
	if errs != nil {
		return nil, errs
	}
	return pinJobs, nil
}

func (pinataCli *PinataCli) GetUserPinnedDataTotal(ids PinataIdentifiers) (UserPinnedDataTotalResult, []error) {
	var pinata Pinata
	result, errs := pinata.getUserPinnedDataTotal(ids)
	return result, errs
}

func (pinataCli *PinataCli) Unpin(ids PinataIdentifiers, criteria UnpinCriteria) (UnpinResult, []error) {
	var pinata Pinata
	result, errs := pinata.unpin(ids, criteria)
	return result, errs
}

func (pinataCli *PinataCli) PinByHash(ids PinataIdentifiers, criteria PinByHashCriteria) (PinByHashResult, []error) {
	var pinata Pinata
	result, errs := pinata.pinByHash(ids, criteria)
	return result, errs
}

func (pinataCli *PinataCli) PinJSON(ids PinataIdentifiers, criteria PinJSONCriteria) (PinJSONResult, []error) {
	var pinata Pinata
	result, errs := pinata.pinJSON(ids, criteria)
	return result, errs
}

func (pinataCli *PinataCli) PinFile(ids PinataIdentifiers, criteria PinFileCriteria) (PinFileResult, []error) {
	var pinata Pinata
	result, errs := pinata.pinFile(ids, criteria)
	return result, errs
}

func (pinataCli *PinataCli) UserPinPolicy(ids PinataIdentifiers, criteria UserPinPolicyCriteria) (UserPinPolicyResult, []error) {
	var pinata Pinata
	result, errs := pinata.userPinPolicy(ids, criteria)
	return result, errs
}
