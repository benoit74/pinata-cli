package pinatacli

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	log "github.com/sirupsen/logrus"
)

type listPinResult struct {
	Count int `json:"count"`
	Rows  []struct {
		ID           string    `json:"id"`
		IpfsPinHash  string    `json:"ipfs_pin_hash"`
		Size         int       `json:"size"`
		UserID       string    `json:"user_id"`
		DatePinned   time.Time `json:"date_pinned"`
		DateUnpinned time.Time `json:"date_unpinned"`
		Metadata     struct {
			Name      string            `json:"name"`
			KeyValues map[string]string `json:"keyvalues"`
		} `json:"metadata"`
		Regions []struct {
			RegionID                string `json:"regionId"`
			DesiredReplicationCount int    `json:"desiredReplicationCount"`
			CurrentReplicationCount int    `json:"currentReplicationCount"`
		} `json:"regions"`
	} `json:"rows"`
}

type ListPinsCriteria struct {
	HashContains   string
	PinStart       string
	PinEnd         string
	UnpinStart     string
	UnpinEnd       string
	PinSizeMin     string
	PinSizeMax     string
	Status         string
	Name           string
	KeyValuesQuery string
}

func (pinata Pinata) getPinsList(ids PinataIdentifiers, criteria ListPinsCriteria) ([]PinataPin, []error) {

	iPage := 0
	var pins []PinataPin

	for {

		query := fmt.Sprintf("https://api.pinata.cloud/data/pinList?pageLimit=%d&pageOffset=%d", pageSize, iPage*pageSize)
		if len(criteria.HashContains) > 0 {
			query += "&hashContains=" + criteria.HashContains
		}
		if len(criteria.PinStart) > 0 {
			query += "&pinStart=" + criteria.PinStart
		}
		if len(criteria.PinEnd) > 0 {
			query += "&pinEnd=" + criteria.PinEnd
		}
		if len(criteria.UnpinStart) > 0 {
			query += "&unpinStart=" + criteria.UnpinStart
		}
		if len(criteria.UnpinEnd) > 0 {
			query += "&unpinEnd=" + criteria.UnpinEnd
		}
		if len(criteria.PinSizeMin) > 0 {
			query += "&pinSizeMin=" + criteria.PinSizeMin
		}
		if len(criteria.PinSizeMax) > 0 {
			query += "&pinSizeMax=" + criteria.PinSizeMax
		}
		if len(criteria.Status) > 0 {
			query += "&status=" + criteria.Status
		}
		if len(criteria.Name) > 0 {
			query += "&metadata[name]=" + criteria.Name
		}
		if len(criteria.KeyValuesQuery) > 0 {
			query += "&metadata[keyvalues]=" + criteria.KeyValuesQuery
		}
		log.Debug("GET ", query)

		req, err := http.NewRequest("GET", query, nil)
		if err != nil {
			return nil, []error{fmt.Errorf("Failed to call Pinata server with GET at %s", query), err}
		}

		req.Header.Add("pinata_api_key", ids.ApiKey)
		req.Header.Add("pinata_secret_api_key", ids.SecretApiKey)

		resp, err := httpClient.Do(req)

		if err != nil {
			return nil, []error{fmt.Errorf("Failed to call Pinata server with GET at %s", query), err}
		}
		defer resp.Body.Close()
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return nil, []error{fmt.Errorf("Failed to read body while calling Pinata server with GET at %s", query), err}
		}

		log.Trace("Status code: ", resp.Status)
		log.Trace("Response body: ", string(body))

		if resp.StatusCode != 200 {
			return nil, []error{fmt.Errorf("Failed to call Pinata server with GET at %s", query),
				fmt.Errorf("Status code: %s", resp.Status),
			}
		}

		var response listPinResult
		err = json.Unmarshal(body, &response)
		if err != nil {
			return nil, []error{fmt.Errorf("Failed to parse JSON response body while calling Pinata server with GET at %s", query), err}
		}

		if len(response.Rows) == 0 {
			log.Debug("No more items found")
			break
		}

		for _, pin := range response.Rows {
			var regions []PinataPinRegion
			for _, region := range pin.Regions {
				regions = append(regions, PinataPinRegion{
					RegionID:                region.RegionID,
					DesiredReplicationCount: region.DesiredReplicationCount,
					CurrentReplicationCount: region.CurrentReplicationCount,
				})
			}
			regions = append(regions)
			pins = append(pins, PinataPin{
				ID:           pin.ID,
				IpfsPinHash:  pin.IpfsPinHash,
				Size:         pin.Size,
				UserID:       pin.UserID,
				DatePinned:   pin.DatePinned,
				DateUnpinned: pin.DateUnpinned,
				Regions:      regions,
				Metadata: PinataPinMetadata{
					Name:      pin.Metadata.Name,
					KeyValues: pin.Metadata.KeyValues,
				},
			})
			log.Trace(pins[len(pins)-1])
		}

		log.Debugf("%d items in list, %d expected", len(pins), response.Count)

		if len(pins) == response.Count {
			log.Debug("All items found")
			break
		}

		iPage++

	}

	return pins, nil
}
