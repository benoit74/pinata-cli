package pinatacli

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	log "github.com/sirupsen/logrus"
)

type pinByHashRequest struct {
	HashToPin      string          `json:"hashToPin"`
	PinataOptions  *pinataOptions  `json:"pinataOptions,omitempty"`
	PinataMetadata *pinataMetadata `json:"pinataMetadata,omitempty"`
}

type pinByHashResult struct {
	ID       string `json:"id"`
	IpfsHash string `json:"IpfsHash"`
	Status   string `json:"Status"`
	Name     string `json:"Name"`
}

type PinByHashCriteria struct {
	HashToPin        string
	HostNodes        []string
	Name             string
	KeyValues        map[string]string
	ReplicationInfos []PinReplicationInfo
}

type PinByHashResult struct {
	ID       string
	IpfsHash string
	Status   string
	Name     string
}

func (criteria PinByHashCriteria) String() string {
	return fmt.Sprintf("HashToPin: %s | HostNodes: %s", criteria.HashToPin, criteria.HostNodes)
}

func (result pinByHashResult) String() string {
	return fmt.Sprintf("ID: %s | IpfsHash: %s | Status: %s | Name: %s",
		result.ID, result.IpfsHash, result.Status, result.Name)
}

func (result PinByHashResult) String() string {
	return fmt.Sprintf("ID: %s | IpfsHash: %s | Status: %s | Name: %s",
		result.ID, result.IpfsHash, result.Status, result.Name)
}

func (pinata Pinata) buildPinByHashRequest(criteria PinByHashCriteria) pinByHashRequest {
	unionCriteria := pinCriteriaUnion{
		HashToPin:        criteria.HashToPin,
		HostNodes:        criteria.HostNodes,
		Name:             criteria.Name,
		KeyValues:        criteria.KeyValues,
		ReplicationInfos: criteria.ReplicationInfos,
	}

	request := pinByHashRequest{
		HashToPin:      criteria.HashToPin,
		PinataOptions:  pinata.buildPinataOptions(unionCriteria),
		PinataMetadata: pinata.buildPinataMetadata(unionCriteria),
	}

	return request
}

func (pinata Pinata) pinByHash(ids PinataIdentifiers, criteria PinByHashCriteria) (PinByHashResult, []error) {

	var result PinByHashResult

	request := pinata.buildPinByHashRequest(criteria)
	requestBody, err := json.Marshal(request)
	if err != nil {
		return result, []error{fmt.Errorf("Failed to marshal criteria to JSON: %s", criteria), err}
	}

	query := "https://api.pinata.cloud/pinning/pinByHash"
	log.Debug("POST ", query)
	log.Trace("Request body: ", string(requestBody))

	req, err := http.NewRequest("POST", query, bytes.NewBuffer(requestBody))
	if err != nil {
		return result, []error{fmt.Errorf("Failed to prepare request for Pinata server with POST at %s", query), err}
	}

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("pinata_api_key", ids.ApiKey)
	req.Header.Add("pinata_secret_api_key", ids.SecretApiKey)

	resp, err := httpClient.Do(req)

	if err != nil {
		return result, []error{fmt.Errorf("Failed to call Pinata server with POST at %s", query), err}
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return result, []error{fmt.Errorf("Failed to read body while calling Pinata server with POST at %s", query), err}
	}

	log.Trace("Status code: ", resp.Status)
	log.Trace("Response body: ", string(body))

	if resp.StatusCode != 200 {
		return result, []error{fmt.Errorf("Error while calling Pinata server with POST at %s", query),
			fmt.Errorf("Status code: %s", resp.Status),
			fmt.Errorf("Response body: %s", string(body)),
		}
	}

	var response pinByHashResult
	err = json.Unmarshal(body, &response)
	if err != nil {
		return result, []error{fmt.Errorf("Failed to parse JSON response body while calling Pinata server with POST at %s", query), err}
	}

	log.Trace("Response unmarshalled: ", response.String())

	result = PinByHashResult{
		ID:       response.ID,
		IpfsHash: response.IpfsHash,
		Status:   response.Status,
		Name:     response.Name,
	}

	log.Trace("Return object: ", result.String())

	return result, nil
}
