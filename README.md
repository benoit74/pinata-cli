# pinata-cli

This application provides a Command Line Interface to Pinata APIs (https://pinata.cloud/).

Consider this as BETA software for now, it is still extremely young software and breaking changes might happen anytime before 1.0 (we are using semantic versionning btw)

## Installation

### Brew

```
brew tap benoit74/benoit74 https://gitlab.com/benoit74/homebrew-benoit74.git
brew install pinata-cli
```

### Build pinata-cli binary directly from source

Requirements :
- git
- go

Build it:
```
git clone git@gitlab.com:benoit74/pinata-cli.git pinata-cli
cd pinata-cli
go build -o bin/pinata ./pinata
```

And enjoy :
```
bin/pinata
```

## Commands

### Available commands

- pin :           Add a hash to Pinata for asynchronous pinning
- list-pin-jobs : List of pin jobs currently ongoing
- list-pins :     List of user pins
- info :          Get total information about your content
- unpin :         Unpin a hash from Pinata
- config :        Set global configuration (global pin policy for now)

### Help

```
pinata --help
```

There is many flags available on each command, so do not forget to run help on each of them for more details on these flags.

### Operation result and logs

Operation result is available on stdout in JSON format.

Logs are available on stderr.

## Store credentials

Credentials to call Pinata APIs are sourced in the following order :
- from two command line flags `--key` and `--secret`
- from two environment variables `PINATA_API_KEY` and `PINATA_SECRET_API_KEY`
- from a configuration file `~/.pinata/credentials` with a specific profile if needed

### Command line flags
```
pinata info --key "your_key" --secret "your secret"
```

### Environment variables
```
export PINATA_API_KEY=anapikey
export PINATA_SECRET_API_KEY=ansupersecretapikeyvalue
```

### Configuration file
Configuration file is written in TOML and supports profiles by using TOML sections.

```
[default]
pinata_api_key="your_key_1"
pinata_secret_api_key="your_secret_1"

[other]
pinata_api_key="your_key_2"
pinata_secret_api_key="your_secret_2"
```

The default profile is "default" and you can select a specific one with the `--profile` flag
```
pinata info --profile other
```

## Example usage

You can pin a file
```
pinata pin --file afile.txt
```

You can pin a whole directory
```
pinata pin --file ../some/where
```

You can wrap the directory name in the parent hash
```
pinata pin --file ../some/where -w
```

You can choose a name for display in Pinata
```
pinata pin --file afile.txt --name "a_name"
```

You can choose add metadata for your own usage
```
pinata pin --file afile.txt --keyvalue key1:value1 --keyvalue key2:value2
```

You can choose to use a specific CID version
```
pinata pin --file afile.txt --cidversion 1
```

You can pin JSON content
```
pinata pin --json '{"some_key":"some_value"}'
```

You can pin a JSON file `file.json`
```
pinata pin --json @file.json
```

You can choose a name for display in Pinata
```
pinata pin --json @file.json --name "a_name"
```

You can choose add metadata for your own usage
```
pinata pin --json @file.json --keyvalue key1:value1 --keyvalue key2:value2
```

You can choose to use a specific CID version
```
pinata pin --json @file.json --cidversion 1
```

You can add a hash to be pinned
```
pinata pin --hash QmdYTBNig2d4dQd5o1LXM3NHbCYA7168NpN5R9m44vDjL7
```

Or add a hash with two nodes addresses to retrieve the hash from
```
pinata pin --hash QmdYTBNig2d4dQd5o1LXM3NHbCYA7168NpN5R9m44vDjL7 --host /ip4/host_node_1_external_IP/tcp/4001/ipfs/host_node_1_peer_id --host /ip4/host_node_2_external_IP/tcp/4001/ipfs/host_node_2_peer_id
```

Or add a hash with a specific replication configuration (example below asks for 2 replication in NYC and 1 in FRA). Note that free account cannot have more than 1 replication.
```
pinata pin --hash QmdYTBNig2d4dQd5o1LXM3NHbCYA7168NpN5R9m44vDjL7 --region NYC1:2 --region FRA1:1
```

And then you can check this hash job
```
pinata list-pin-jobs --hash QmdYTBNig2d4dQd5o1LXM3NHbCYA7168NpN5R9m44vDjL7
```

Or list the corresponding pin status
```
pinata list-pins --hash QmdYTBNig2d4dQd5o1LXM3NHbCYA7168NpN5R9m44vDjL7
```

Or list all your pins
```
pinata list-pins
```

Or only your _active_ pins
```
pinata list-pins --status pinned
```

Get info about your total usage
```
pinata info
```

And finally unpin a hash
```
pinata unpin --hash QmdYTBNig2d4dQd5o1LXM3NHbCYA7168NpN5R9m44vDjL7
```

You can also alter your global pin replication config (example below asks for 2 replication in NYC and 1 in FRA). Note that free account cannot have more than 1 replication.
```
pinata config --region NYC1:2 --region FRA1:1
```

## Bugs

Should you encounter a bug, please check if there is already an opened similar issue on this Gitlab repository. If not, please open a new issue.