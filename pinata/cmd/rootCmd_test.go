package cmd

import (
	"reflect"
	"testing"

	pinatacli "gitlab.com/benoit74/pinata-cli"
)

func Test_parseRegionsToReplicationInfo(t *testing.T) {
	tests := []struct {
		name    string
		regions []string
		want    []pinatacli.PinReplicationInfo
		wantErr bool
	}{
		{
			name:    "zeroRegion",
			regions: nil,
			want:    nil,
			wantErr: false,
		},
		{
			name:    "oneRegion",
			regions: []string{"FRA1:1"},
			want: []pinatacli.PinReplicationInfo{
				pinatacli.PinReplicationInfo{
					Region:           "FRA1",
					ReplicationCount: 1,
				},
			},
			wantErr: false,
		},
		{
			name:    "twoRegion",
			regions: []string{"FRA1:2", "FRA2:1"},
			want: []pinatacli.PinReplicationInfo{
				pinatacli.PinReplicationInfo{
					Region:           "FRA1",
					ReplicationCount: 2,
				},
				pinatacli.PinReplicationInfo{
					Region:           "FRA2",
					ReplicationCount: 1,
				},
			},
			wantErr: false,
		},
		{
			name:    "countIsNotAnInt",
			regions: []string{"FRA1:A"},
			want:    nil,
			wantErr: true,
		},
		{
			name:    "delimitermissing",
			regions: []string{"FRA1"},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, gotErrs := parseRegionsToReplicationInfo(tt.regions)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("parseRegionsToReplicationInfo()\nwant = %v\ngot = %v", tt.want, got)
			}
			if (len(gotErrs) > 0) != tt.wantErr {
				t.Errorf("parseRegionsToReplicationInfo()\nwantErr = %v\nerrors = %v", tt.wantErr, gotErrs)
				return
			}
		})
	}
}

func Test_parseKeyValues(t *testing.T) {
	tests := []struct {
		name      string
		keyvalues []string
		want      map[string]string
		wantErr   bool
	}{
		{
			name:      "zeroKeyValue",
			keyvalues: nil,
			want:      nil,
			wantErr:   false,
		},
		{
			name:      "oneKeyValue",
			keyvalues: []string{"key1:value1"},
			want: map[string]string{
				"key1": "value1",
			},
			wantErr: false,
		},
		{
			name:      "twoKeyValues",
			keyvalues: []string{"key1:value1", "key2:value2"},
			want: map[string]string{
				"key1": "value1",
				"key2": "value2",
			},
			wantErr: false,
		},
		{
			name:      "redefine same key twice",
			keyvalues: []string{"key1:value1", "key1:value2"},
			want:      nil,
			wantErr:   true,
		},
		{
			name:      "wrong format",
			keyvalues: []string{"key1,value1"},
			want:      nil,
			wantErr:   true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, gotErrs := parseKeyValues(tt.keyvalues)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("parseKeyValues()\nwant = %v\ngot = %v", tt.want, got)
			}
			if (len(gotErrs) > 0) != tt.wantErr {
				t.Errorf("parseKeyValues()\nwantErr = %v\nerrors = %v", tt.wantErr, gotErrs)
				return
			}
		})
	}
}
