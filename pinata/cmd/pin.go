package cmd

import (
	"encoding/json"
	"fmt"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	pinatacli "gitlab.com/benoit74/pinata-cli"
)

var (
	pinCmd = &cobra.Command{
		Use:   "pin",
		Short: "Add a hash to Pinata for asynchronous pinning",
		Long:  `Add a hash to Pinata for asynchronous pinning. Content added through this endpoint is pinned in the background and will show up in your pinned items once the content has been found / pinned. For this operation to succeed, the content for the hash you provide must already be pinned by another node on the IFPS network.`,
		Run: func(cmd *cobra.Command, args []string) {

			prepare()

			if hash == "" && jsonp == "" && file == "" {
				log.Error("Either hash or JSON or File to pin is mandatory")
				return
			}

			var pinata pinatacli.PinataCli

			regions, errs := parseRegionsToReplicationInfo(regions)

			for _, err := range errs {
				log.Error(err)
			}

			if len(errs) > 0 {
				return
			}

			keyvalues, errs := parseKeyValues(keyvalues)

			for _, err := range errs {
				log.Error(err)
			}

			if len(errs) > 0 {
				return
			}

			var result interface{}
			if jsonp != "" {
				criteria := pinatacli.PinJSONCriteria{
					JSON:             jsonp,
					CIDVersion:       cidVersion,
					ReplicationInfos: regions,
					Name:             name,
					KeyValues:        keyvalues,
				}
				result, errs = pinata.PinJSON(ids, criteria)
			} else if file != "" {
				criteria := pinatacli.PinFileCriteria{
					File:              file,
					CIDVersion:        cidVersion,
					WrapWithDirectory: wrap,
					ReplicationInfos:  regions,
					Name:              name,
					KeyValues:         keyvalues,
				}
				result, errs = pinata.PinFile(ids, criteria)
			} else {
				criteria := pinatacli.PinByHashCriteria{
					HashToPin:        hash,
					HostNodes:        hosts,
					ReplicationInfos: regions,
					Name:             name,
					KeyValues:        keyvalues,
				}
				result, errs = pinata.PinByHash(ids, criteria)
			}

			for _, err := range errs {
				log.Error(err)
			}

			if len(errs) > 0 {
				return
			}

			byteData, err := json.MarshalIndent(result, "", "  ")
			if err != nil {
				log.Error(err)
				return
			}
			fmt.Println(string(byteData))

		},
	}
)

func init() {

	pinCmd.Flags().StringVarP(&apiKey, "key", "k", "", "API Key (MANDATORY)")
	pinCmd.Flags().StringVarP(&secretApiKey, "secret", "s", "", "Secret API Key (MANDATORY)")
	pinCmd.Flags().StringVarP(&hash, "hash", "", "", "Hash to pin to IPFS (MANDATORY)")
	pinCmd.Flags().StringVarP(&jsonp, "json", "", "", "JSON data to pin to IPFS")
	pinCmd.Flags().StringVarP(&file, "file", "", "", "File or directory to pin to IPFS")
	pinCmd.Flags().BoolVarP(&wrap, "wrap", "w", false, "Wrap with directory")
	pinCmd.Flags().StringVarP(&cidVersion, "cidversion", "", "", "CID version (not when pinning by hash), '1' or '0'")
	pinCmd.Flags().StringArrayVarP(&hosts, "host", "", nil, "Host nodes to retrieved the hash from")
	pinCmd.Flags().StringArrayVarP(&regions, "region", "", nil, "Pin on specific regions with specific replication count")
	pinCmd.Flags().StringVarP(&name, "name", "", "", "A name for this hash, for display in Pinata only")
	pinCmd.Flags().StringArrayVarP(&keyvalues, "keyvalue", "", nil, "Additional key/value to store in Pinata")
	pinCmd.Flags().StringVarP(&verbosity, "verbosity", "v", "INFO", "Verbosity of logs sent to stderr (TRACE,DEBUG,INFO,WARN,ERROR,FATAL,PANIC)")
	pinCmd.Flags().StringVarP(&profile, "profile", "", "default", "Profile to use in ~/.pinata/credentials")
	rootCmd.AddCommand(pinCmd)

}
