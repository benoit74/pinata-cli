package cmd

import (
	"encoding/json"
	"fmt"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	pinatacli "gitlab.com/benoit74/pinata-cli"
)

var (
	configCmd = &cobra.Command{
		Use:   "config",
		Short: "Set global configuration (global pin policy for now)",
		Long:  `Set the global configuration of your account, composed for now of the global pin policy.`,
		Run: func(cmd *cobra.Command, args []string) {

			prepare()

			log.Debug("Running config")

			if len(regions) == 0 {
				log.Error("Specify at least one region")
				return
			}

			regions, errs := parseRegionsToReplicationInfo(regions)

			for _, err := range errs {
				log.Error(err)
			}

			if len(errs) > 0 {
				return
			}

			var pinata pinatacli.PinataCli

			criteria := pinatacli.UserPinPolicyCriteria{
				ReplicationInfos: regions,
			}

			result, errs := pinata.UserPinPolicy(ids, criteria)

			for _, err := range errs {
				log.Error(err)
			}

			if len(errs) > 0 {
				return
			}

			byteData, err := json.MarshalIndent(result, "", "  ")
			if err != nil {
				log.Error(err)
				return
			}
			fmt.Println(string(byteData))

		},
	}
)

func init() {
	configCmd.Flags().StringVarP(&apiKey, "key", "k", "", "API Key (MANDATORY)")
	configCmd.Flags().StringVarP(&secretApiKey, "secret", "s", "", "Secret API Key (MANDATORY)")
	configCmd.Flags().StringVarP(&verbosity, "verbosity", "v", "INFO", "Verbosity of logs sent to stderr (TRACE,DEBUG,INFO,WARN,ERROR,FATAL,PANIC)")
	configCmd.Flags().StringVarP(&profile, "profile", "", "default", "Profile to use in ~/.pinata/credentials")
	configCmd.Flags().StringArrayVarP(&regions, "region", "", nil, "Specify level of replication by region, format is <region>:<count>")
	rootCmd.AddCommand(configCmd)
}
