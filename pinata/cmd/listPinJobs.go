package cmd

import (
	"encoding/json"
	"fmt"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	pinatacli "gitlab.com/benoit74/pinata-cli"
)

type listPinJobsResult struct {
	Count int
	Items []pinatacli.PinataPinJob
}

var (
	listPinJobsCmd = &cobra.Command{
		Use:   "list-pin-jobs",
		Short: "List of pin jobs currently ongoing.",
		Long:  `Retrieves a list of all the pins that are currently in the pin queue for your user.`,
		Run: func(cmd *cobra.Command, args []string) {

			prepare()

			var pinata pinatacli.PinataCli

			criteria := pinatacli.ListPinJobsCriteria{
				IpfsPinHash: hash,
				Sort:        sort,
				Status:      status,
			}
			pinJobs, errs := pinata.GetPinJobsList(ids, criteria)

			for _, err := range errs {
				log.Error(err)
			}

			if len(errs) > 0 {
				return
			}

			byteData, err := json.MarshalIndent(listPinJobsResult{
				Count: len(pinJobs),
				Items: pinJobs,
			}, "", "  ")
			if err != nil {
				log.Error(err)
				return
			}
			fmt.Println(string(byteData))

		},
	}
)

func init() {
	listPinJobsCmd.Flags().StringVarP(&apiKey, "key", "k", "", "API Key (MANDATORY)")
	listPinJobsCmd.Flags().StringVarP(&secretApiKey, "secret", "s", "", "Secret API Key (MANDATORY)")
	listPinJobsCmd.Flags().StringVarP(&hash, "hash", "", "", "Retrieve the record for a specific IPFS hash")
	listPinJobsCmd.Flags().StringVarP(&sort, "sort", "", "", "Sort the results by the date added to the pinning queue (ASC|DESC	)")
	listPinJobsCmd.Flags().StringVarP(&status, "status", "", "", "Filter by the status of the job in the pinning queue  (searching|expired|over_free_limit|over_max_size|invalid_object|bad_host_node)")
	listPinJobsCmd.Flags().StringVarP(&verbosity, "verbosity", "v", "INFO", "Verbosity of logs sent to stderr (TRACE,DEBUG,INFO,WARN,ERROR,FATAL,PANIC)")
	listPinJobsCmd.Flags().StringVarP(&profile, "profile", "", "default", "Profile to use in ~/.pinata/credentials")
	rootCmd.AddCommand(listPinJobsCmd)
}
