package cmd

import (
	"encoding/json"
	"fmt"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	pinatacli "gitlab.com/benoit74/pinata-cli"
)

var (
	infoCmd = &cobra.Command{
		Use:   "info",
		Short: "Get total information about your content",
		Long:  `This endpoint returns the total information (count + size) for all content that you've pinned through Pinata.`,
		Run: func(cmd *cobra.Command, args []string) {

			prepare()

			log.Debug("Running info")

			var pinata pinatacli.PinataCli

			result, errs := pinata.GetUserPinnedDataTotal(ids)

			for _, err := range errs {
				log.Error(err)
			}

			if len(errs) > 0 {
				return
			}

			byteData, err := json.MarshalIndent(result, "", "  ")
			if err != nil {
				log.Error(err)
				return
			}
			fmt.Println(string(byteData))

		},
	}
)

func init() {
	infoCmd.Flags().StringVarP(&apiKey, "key", "k", "", "API Key (MANDATORY)")
	infoCmd.Flags().StringVarP(&secretApiKey, "secret", "s", "", "Secret API Key (MANDATORY)")
	infoCmd.Flags().StringVarP(&verbosity, "verbosity", "v", "INFO", "Verbosity of logs sent to stderr (TRACE,DEBUG,INFO,WARN,ERROR,FATAL,PANIC)")
	infoCmd.Flags().StringVarP(&profile, "profile", "", "default", "Profile to use in ~/.pinata/credentials")
	rootCmd.AddCommand(infoCmd)
}
