package cmd

import (
	"encoding/json"
	"fmt"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	pinatacli "gitlab.com/benoit74/pinata-cli"
)

var (
	unpinCmd = &cobra.Command{
		Use:   "unpin",
		Short: "Unpin a hash from Pinata",
		Long:  `Unpin a hash previously pinned to Pinata.`,
		Run: func(cmd *cobra.Command, args []string) {

			prepare()

			log.Debug("Running unpin")

			if hash == "" {
				log.Error("Hash to pin is mandatory")
				return
			}

			var pinata pinatacli.PinataCli

			criteria := pinatacli.UnpinCriteria{
				HashToRemove: hash,
			}
			result, errs := pinata.Unpin(ids, criteria)

			for _, err := range errs {
				log.Error(err)
			}

			if len(errs) > 0 {
				return
			}

			byteData, err := json.MarshalIndent(result, "", "  ")
			if err != nil {
				log.Error(err)
				return
			}
			fmt.Println(string(byteData))

		},
	}
)

func init() {
	unpinCmd.Flags().StringVarP(&apiKey, "key", "k", "", "API Key (MANDATORY)")
	unpinCmd.Flags().StringVarP(&secretApiKey, "secret", "s", "", "Secret API Key (MANDATORY)")
	unpinCmd.Flags().StringVarP(&hash, "hash", "", "", "Hash to pin to IPFS (MANDATORY)")
	unpinCmd.Flags().StringVarP(&verbosity, "verbosity", "v", "INFO", "Verbosity of logs sent to stderr (TRACE,DEBUG,INFO,WARN,ERROR,FATAL,PANIC)")
	unpinCmd.Flags().StringVarP(&profile, "profile", "", "default", "Profile to use in ~/.pinata/credentials")
	rootCmd.AddCommand(unpinCmd)
}
