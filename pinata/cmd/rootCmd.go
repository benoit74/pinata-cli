package cmd

import (
	"fmt"
	"os"
	"strconv"
	"strings"

	"github.com/mitchellh/go-homedir"
	"github.com/pelletier/go-toml"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	pinatacli "gitlab.com/benoit74/pinata-cli"
)

const (
	InfoColor    = "\033[1;34m%s\033[0m"
	NoticeColor  = "\033[1;36m%s\033[0m"
	WarningColor = "\033[1;33m%s\033[0m"
	ErrorColor   = "\033[1;31m%s\033[0m"
	DebugColor   = "\033[0;36m%s\033[0m"
)

var (
	apiKey         string
	secretApiKey   string
	hash           string
	jsonp          string
	file           string
	cidVersion     string
	name           string
	profile        string
	keyvaluesquery string
	sort           string
	hosts          []string
	regions        []string
	keyvalues      []string
	pinStart       string
	pinEnd         string
	unpinStart     string
	unpinEnd       string
	pinSizeMin     string
	pinSizeMax     string
	status         string
	verbosity      string
	wrap           bool

	ids pinatacli.PinataIdentifiers

	rootCmd = &cobra.Command{
		Use:     "pinata",
		Short:   "pinata handles operations on pinata.cloud",
		Long:    `pinata handles operations on pinata.cloud`,
		Version: "0.2.1",
	}
)

func prepare() {
	parseGenericFlags()
	sourceCredentials()
}

func sourceCredentialsFromFile() {
	var value string
	var ok bool

	credentialFile, err := homedir.Expand("~/.pinata/credentials")
	if err != nil {
		log.Warnf("Error while looking for your home directory, credentials won't be sourced from file: %s", err)
		return
	}
	if _, err := os.Stat(credentialFile); os.IsNotExist(err) {
		log.Trace("credentials file not found")
		return
	}
	config, err := toml.LoadFile(credentialFile)
	if err != nil {
		log.Warnf("Error while parsing credentials file, credentials won't be sourced from file: %s", err)
		return
	}
	if !config.Has(profile) {
		log.Warnf("Profile '%s' in not present in credentials file", profile)
		return
	}
	log.Trace("profile: ", profile)
	value, ok = config.Get(fmt.Sprintf("%s.pinata_api_key", profile)).(string)
	if !ok {
		log.Warnf("Error while parsing credentials file, key pinata_api_key of profile %s is not a string", profile)
		return
	}
	if value != "" {
		ids.ApiKey = value
	}
	value, ok = config.Get(fmt.Sprintf("%s.pinata_secret_api_key", profile)).(string)
	if !ok {
		log.Warnf("Error while parsing credentials file, key pinata_api_key of profile %s is not a string", profile)
		return
	}
	if value != "" {
		ids.SecretApiKey = value
	}

}

func sourceCredentials() {
	var value string

	// source credentials from file
	sourceCredentialsFromFile()

	// source credentials from environment
	value = os.Getenv("PINATA_API_KEY")
	if value != "" {
		ids.ApiKey = value
	}
	value = os.Getenv("PINATA_SECRET_API_KEY")
	if value != "" {
		ids.SecretApiKey = value
	}

	// source credentials from flags
	value = apiKey
	if value != "" {
		ids.ApiKey = value
	}
	value = secretApiKey
	if value != "" {
		ids.SecretApiKey = value
	}

	// Check that credentials have been provided
	if ids.ApiKey == "" {
		log.Error("API Key is mandatory")
		return
	}

	if ids.SecretApiKey == "" {
		log.Error("Secret API Key is mandatory")
		return
	}
}

func parseGenericFlags() {

	switch strings.ToUpper(verbosity) {
	case "TRACE":
		log.SetLevel(log.TraceLevel)
	case "DEBUG":
		log.SetLevel(log.DebugLevel)
	case "INFO":
		log.SetLevel(log.InfoLevel)
	case "WARN":
		log.SetLevel(log.WarnLevel)
	case "ERROR":
		log.SetLevel(log.ErrorLevel)
	case "FATAL":
		log.SetLevel(log.FatalLevel)
	case "PANIC":
		log.SetLevel(log.PanicLevel)
	default:
		log.SetLevel(log.InfoLevel)
	}

}

func parseRegionsToReplicationInfo(regions []string) ([]pinatacli.PinReplicationInfo, []error) {
	var result []pinatacli.PinReplicationInfo
	for _, region := range regions {
		tokens := strings.Split(region, ":")
		if len(tokens) != 2 {
			return nil, []error{fmt.Errorf("Failed to split region '%s'. Expected format : <region>:<count>", region)}
		}
		count, err := strconv.Atoi(tokens[1])
		if err != nil {
			return result, []error{fmt.Errorf("Failed to parse int value from '%s'", tokens[1]), err}
		}
		result = append(result, pinatacli.PinReplicationInfo{
			Region:           tokens[0],
			ReplicationCount: count,
		})
	}
	return result, nil
}

func parseKeyValues(keyvalues []string) (map[string]string, []error) {
	var result map[string]string
	if len(keyvalues) > 0 {
		result = make(map[string]string)
	}
	for _, keyvalue := range keyvalues {
		tokens := strings.Split(keyvalue, ":")
		if len(tokens) != 2 {
			return nil, []error{fmt.Errorf("Failed to split keyvalue '%s'. Expected format : <key>:<value>", keyvalue)}
		}
		if _, ok := result[tokens[0]]; ok {
			return nil, []error{fmt.Errorf("Token '%s' is defined twice", tokens[0])}
		}
		result[tokens[0]] = tokens[1]
	}
	return result, nil
}

func Execute() error {
	return rootCmd.Execute()
}
