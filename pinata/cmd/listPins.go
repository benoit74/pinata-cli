package cmd

import (
	"encoding/json"
	"fmt"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	pinatacli "gitlab.com/benoit74/pinata-cli"
)

type listPinResult struct {
	Count int
	Items []pinatacli.PinataPin
}

var (
	listPinsCmd = &cobra.Command{
		Use:   "list-pins",
		Short: "List of user pins.",
		Long:  `This endpoint returns data on what content the sender has pinned to IPFS through Pinata.`,
		Run: func(cmd *cobra.Command, args []string) {

			prepare()

			log.Debug("Running list-pins")

			var pinata pinatacli.PinataCli

			criteria := pinatacli.ListPinsCriteria{
				HashContains:   hash,
				PinStart:       pinStart,
				PinEnd:         pinEnd,
				UnpinStart:     unpinStart,
				UnpinEnd:       unpinEnd,
				PinSizeMin:     pinSizeMin,
				PinSizeMax:     pinSizeMax,
				Status:         status,
				Name:           name,
				KeyValuesQuery: keyvaluesquery,
			}
			pins, errs := pinata.GetPinsList(ids, criteria)

			for _, err := range errs {
				log.Error(err)
			}

			if len(errs) > 0 {
				return
			}

			byteData, err := json.MarshalIndent(listPinResult{
				Count: len(pins),
				Items: pins,
			}, "", "  ")
			if err != nil {
				log.Error(err)
				return
			}
			fmt.Println(string(byteData))

		},
	}
)

func init() {
	listPinsCmd.Flags().StringVarP(&apiKey, "key", "k", "", "API Key (MANDATORY)")
	listPinsCmd.Flags().StringVarP(&secretApiKey, "secret", "s", "", "Secret API Key (MANDATORY)")
	listPinsCmd.Flags().StringVarP(&hash, "hash", "", "", "Filter on alphanumeric characters inside of pin hashes.")
	listPinsCmd.Flags().StringVarP(&pinStart, "pin-start", "", "", "Exclude pin records that were pinned before the passed in datetime")
	listPinsCmd.Flags().StringVarP(&pinEnd, "pin-end", "", "", "Exclude pin records that were pinned after the passed in datetime")
	listPinsCmd.Flags().StringVarP(&unpinStart, "unpin-start", "", "", "Exclude pin records that were unpinned before the passed in datetime")
	listPinsCmd.Flags().StringVarP(&unpinEnd, "unpin-end", "", "", "Exclude pin records that were unpinned after the passed in datetime")
	listPinsCmd.Flags().StringVarP(&pinSizeMin, "size-min", "", "", "The minimum byte size that pin record you're looking for can have")
	listPinsCmd.Flags().StringVarP(&pinSizeMax, "size-max", "", "", "The maximum byte size that pin record you're looking for can have")
	listPinsCmd.Flags().StringVarP(&status, "status", "", "", "Status of pins to list (all|pinned|unpinned)")
	listPinsCmd.Flags().StringVarP(&name, "name", "", "", "Name of the hash to search")
	listPinsCmd.Flags().StringVarP(&keyvaluesquery, "keyvaluesquery", "", "", "Additional key/value query (raw)")
	listPinsCmd.Flags().StringVarP(&verbosity, "verbosity", "v", "INFO", "Verbosity of logs sent to stderr (TRACE,DEBUG,INFO,WARN,ERROR,FATAL,PANIC)")
	listPinsCmd.Flags().StringVarP(&profile, "profile", "", "default", "Profile to use in ~/.pinata/credentials")
	rootCmd.AddCommand(listPinsCmd)
}
