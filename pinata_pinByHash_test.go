package pinatacli

import (
	"encoding/json"
	"reflect"
	"testing"
)

func TestPinata_buildPinByHashRequest(t *testing.T) {
	type args struct {
		criteria PinByHashCriteria
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "a simple hash to pin",
			args: args{
				criteria: PinByHashCriteria{
					HashToPin: "a_hash_to_pin",
				},
			},
			want: `{"hashToPin":"a_hash_to_pin"}`,
		},
		{
			name: "a hash to pin with one host",
			args: args{
				criteria: PinByHashCriteria{
					HashToPin: "a_hash_to_pin",
					HostNodes: []string{"a_host"},
				},
			},
			want: `{"hashToPin":"a_hash_to_pin","pinataOptions":{"hostNodes":["a_host"]}}`,
		},
		{
			name: "a hash to pin with two hosts",
			args: args{
				criteria: PinByHashCriteria{
					HashToPin: "a_hash_to_pin",
					HostNodes: []string{"a_host", "another_host"},
				},
			},
			want: `{"hashToPin":"a_hash_to_pin","pinataOptions":{"hostNodes":["a_host","another_host"]}}`,
		},
		{
			name: "a hash with one replication option",
			args: args{
				criteria: PinByHashCriteria{
					HashToPin: "a_hash_to_pin",
					ReplicationInfos: []PinReplicationInfo{
						PinReplicationInfo{
							Region:           "FRA2",
							ReplicationCount: 123,
						},
					},
				},
			},
			want: `{"hashToPin":"a_hash_to_pin","pinataOptions":{"customPinPolicy":{"regions":[{"id":"FRA2","desiredReplicationCount":123}]}}}`,
		},
		{
			name: "a hash with two replication option",
			args: args{
				criteria: PinByHashCriteria{
					HashToPin: "a_hash_to_pin",
					ReplicationInfos: []PinReplicationInfo{
						PinReplicationInfo{
							Region:           "FRA2",
							ReplicationCount: 123,
						},
						PinReplicationInfo{
							Region:           "ZZZ8",
							ReplicationCount: 234,
						},
					},
				},
			},
			want: `{"hashToPin":"a_hash_to_pin","pinataOptions":{"customPinPolicy":{"regions":[{"id":"FRA2","desiredReplicationCount":123},{"id":"ZZZ8","desiredReplicationCount":234}]}}}`,
		},
		{
			name: "a hash with two nodes and two replication option",
			args: args{
				criteria: PinByHashCriteria{
					HashToPin: "a_hash_to_pin",
					HostNodes: []string{"a_host", "another_host"},
					ReplicationInfos: []PinReplicationInfo{
						PinReplicationInfo{
							Region:           "FRA2",
							ReplicationCount: 123,
						},
						PinReplicationInfo{
							Region:           "ZZZ8",
							ReplicationCount: 234,
						},
					},
				},
			},
			want: `{"hashToPin":"a_hash_to_pin","pinataOptions":{"hostNodes":["a_host","another_host"],"customPinPolicy":{"regions":[{"id":"FRA2","desiredReplicationCount":123},{"id":"ZZZ8","desiredReplicationCount":234}]}}}`,
		},
		{
			name: "a hash with a name",
			args: args{
				criteria: PinByHashCriteria{
					HashToPin: "a_hash_to_pin",
					Name:      "a_name",
				},
			},
			want: `{"hashToPin":"a_hash_to_pin","pinataMetadata":{"name":"a_name"}}`,
		},
		{
			name: "a hash with a name and keyvalues",
			args: args{
				criteria: PinByHashCriteria{
					HashToPin: "a_hash_to_pin",
					Name:      "a_name",
					KeyValues: map[string]string{"key1": "value1"},
				},
			},
			want: `{"hashToPin":"a_hash_to_pin","pinataMetadata":{"name":"a_name","keyvalues":{"key1":"value1"}}}`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			pinata := Pinata{}
			got := pinata.buildPinByHashRequest(tt.args.criteria)
			gotBytes, err := json.Marshal(got)
			if err != nil {
				t.Errorf("Pinata.buildPinByHashRequest()\ngot unexpected error %v", err)
			}
			gotStr := string(gotBytes)
			if !reflect.DeepEqual(gotStr, tt.want) {
				t.Errorf("Pinata.buildPinByHashRequest()\ngot  = %v\nwant = %v", gotStr, tt.want)
			}
		})
	}
}
