package pinatacli

import (
	"testing"
)

func TestPinata_hasPinataContent(t *testing.T) {
	type args struct {
		jsonp string
	}
	tests := []struct {
		name    string
		args    args
		want    bool
		wantErr bool
	}{
		{
			name: "simple positive case",
			args: args{
				jsonp: `{"pinataContent":{"someField":"someValue"}}`,
			},
			want:    true,
			wantErr: false,
		},
		{
			name: "complex positive case",
			args: args{
				jsonp: `{"something":{"bla":"blu"},"pinataContent":{"someField":"someValue"}}`,
			},
			want:    true,
			wantErr: false,
		},
		{
			name: "complex positive case with array as content",
			args: args{
				jsonp: `{"something":{"bla":"blu"},"pinataContent":[1, 2, 3]}`,
			},
			want:    true,
			wantErr: false,
		},
		{
			name: "simple negative case",
			args: args{
				jsonp: `{"something":{"bla":"blu"}}`,
			},
			want:    false,
			wantErr: false,
		},
		{
			name: "complex negative case",
			args: args{
				jsonp: `[{"something":{"bla":"blu"}}]`,
			},
			want:    false,
			wantErr: false,
		},
		{
			name: "empty json",
			args: args{
				jsonp: `{}`,
			},
			want:    false,
			wantErr: false,
		},
		{
			name: "invalid json",
			args: args{
				jsonp: `{bla}`,
			},
			want:    false,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			pinata := Pinata{}
			got, gotErrs := pinata.hasPinataContent([]byte(tt.args.jsonp))
			if (len(gotErrs) > 0) != tt.wantErr {
				t.Errorf("Pinata.hasPinataContent()\nwantErr = %v\nerrors = %v", tt.wantErr, gotErrs)
				return
			}
			if got != tt.want {
				t.Errorf("Pinata.hasPinataContent() got = %v, want %v", got, tt.want)
			}
		})
	}
}
