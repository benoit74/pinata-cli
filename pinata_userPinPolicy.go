package pinatacli

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	log "github.com/sirupsen/logrus"
)

type userPinPolicyRequest struct {
	NewPinPolicy *newPinPolicy `json:"newPinPolicy"`
}

type newPinPolicy struct {
	Regions []region `json:"regions"`
}

type UserPinPolicyCriteria struct {
	ReplicationInfos []PinReplicationInfo
}

type UserPinPolicyResult struct {
	Success bool `json:"success"`
}

func (criteria UserPinPolicyCriteria) String() string {
	res := ""
	for _, value := range criteria.ReplicationInfos {
		res = fmt.Sprintf("%s%s:%d, ", res, value.Region, value.ReplicationCount)
	}
	return res
}

func (pinata Pinata) userPinPolicy(ids PinataIdentifiers, criteria UserPinPolicyCriteria) (UserPinPolicyResult, []error) {

	var result = UserPinPolicyResult{Success: false}

	if len(criteria.ReplicationInfos) == 0 {
		return result, []error{fmt.Errorf("Please specify at least one region")}
	}

	policy := newPinPolicy{}
	for _, info := range criteria.ReplicationInfos {
		policy.Regions = append(policy.Regions, region{
			ID:                      info.Region,
			DesiredReplicationCount: info.ReplicationCount,
		})
	}

	request := userPinPolicyRequest{
		NewPinPolicy: &policy,
	}

	requestBody, err := json.Marshal(request)
	if err != nil {
		return result, []error{fmt.Errorf("Failed to marshal criteria to JSON: %s", criteria), err}
	}

	query := "https://api.pinata.cloud/pinning/userPinPolicy"
	log.Debug("PUT ", query)
	log.Trace("Request body: ", string(requestBody))

	req, err := http.NewRequest("PUT", query, bytes.NewBuffer(requestBody))
	if err != nil {
		return result, []error{fmt.Errorf("Failed to prepare request for Pinata server with PUT at %s", query), err}
	}

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("pinata_api_key", ids.ApiKey)
	req.Header.Add("pinata_secret_api_key", ids.SecretApiKey)

	resp, err := httpClient.Do(req)

	if err != nil {
		return result, []error{fmt.Errorf("Failed to call Pinata server with PUT at %s", query), err}
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return result, []error{fmt.Errorf("Failed to read body while calling Pinata server with POST at %s", query), err}
	}

	log.Trace("Status code: ", resp.Status)
	log.Trace("Response body: ", string(body))

	if resp.StatusCode != 200 {
		return result, nil
	}

	return UserPinPolicyResult{Success: true}, nil
}
