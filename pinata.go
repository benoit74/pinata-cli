package pinatacli

import (
	"net/http"
)

const (
	pageSize = 100
)

var httpClient *http.Client

func init() {
	httpClient = &http.Client{}
}

// Pinata object, implementing all operations on the Pinata API
type Pinata struct{}

type pinataOptions struct {
	CIDVersion        string           `json:"cidVersion,omitempty"`
	WrapWithDirectory *bool            `json:"wrapWithDirectory,omitempty"`
	HostNodes         []string         `json:"hostNodes,omitempty"`
	CustomPinPolicy   *customPinPolicy `json:"customPinPolicy,omitempty"`
}

type pinataMetadata struct {
	Name      string            `json:"name,omitempty"`
	KeyValues map[string]string `json:"keyvalues,omitempty"`
}

type customPinPolicy struct {
	Regions []region `json:"regions,omitempty"`
}

type region struct {
	ID                      string `json:"id"`
	DesiredReplicationCount int    `json:"desiredReplicationCount"`
}

type pinCriteriaUnion struct {
	HashToPin         string
	HostNodes         []string
	CIDVersion        string
	WrapWithDirectory *bool
	Name              string
	KeyValues         map[string]string
	ReplicationInfos  []PinReplicationInfo
}

func (pinata Pinata) buildPinataOptions(criteria pinCriteriaUnion) *pinataOptions {
	var options pinataOptions

	if criteria.WrapWithDirectory == nil && criteria.CIDVersion == "" && len(criteria.HostNodes) == 0 && len(criteria.ReplicationInfos) == 0 {
		return nil
	}

	if criteria.WrapWithDirectory != nil {
		options.WrapWithDirectory = criteria.WrapWithDirectory
	}

	if criteria.CIDVersion != "" {
		options.CIDVersion = criteria.CIDVersion
	}

	if len(criteria.HostNodes) > 0 {
		options.HostNodes = criteria.HostNodes
	}
	if len(criteria.ReplicationInfos) > 0 {
		policy := customPinPolicy{}
		for _, info := range criteria.ReplicationInfos {
			policy.Regions = append(policy.Regions, region{
				ID:                      info.Region,
				DesiredReplicationCount: info.ReplicationCount,
			})
		}
		options.CustomPinPolicy = &policy
	}

	return &options

}

func (pinata Pinata) buildPinataMetadata(criteria pinCriteriaUnion) *pinataMetadata {

	var metadata pinataMetadata

	if len(criteria.Name) == 0 && len(criteria.KeyValues) == 0 {
		return nil
	}

	metadata.Name = criteria.Name
	metadata.KeyValues = criteria.KeyValues

	return &metadata

}
