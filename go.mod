module gitlab.com/benoit74/pinata-cli

go 1.13

require (
	github.com/mitchellh/go-homedir v1.1.0
	github.com/pelletier/go-toml v1.2.0
	github.com/sirupsen/logrus v1.4.2
	github.com/spf13/cobra v0.0.5
)
