package pinatacli

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
)

type pinJSONRequest struct {
	PinataOptions  *pinataOptions  `json:"pinataOptions,omitempty"`
	PinataMetadata *pinataMetadata `json:"pinataMetadata,omitempty"`
	PinataContent  interface{}     `json:"pinataContent"`
}

type pinJSONResult struct {
	IpfsHash  string    `json:"IpfsHash"`
	PinSize   int       `json:"PinSize"`
	Timestamp time.Time `json:"Timestamp"`
}

type PinJSONCriteria struct {
	JSON             string
	CIDVersion       string
	Name             string
	KeyValues        map[string]string
	ReplicationInfos []PinReplicationInfo
}

type PinJSONResult struct {
	IpfsHash  string
	PinSize   int
	Timestamp time.Time
}

func (result pinJSONResult) String() string {
	return fmt.Sprintf("IpfsHash: %s | PinSize: %d | Timestamp: %s",
		result.IpfsHash, result.PinSize, result.Timestamp)
}

func (result PinJSONResult) String() string {
	return fmt.Sprintf("IpfsHash: %s | PinSize: %d | Timestamp: %s",
		result.IpfsHash, result.PinSize, result.Timestamp)
}

func (pinata Pinata) getJSONContent(criteria PinJSONCriteria) ([]byte, []error) {

	var content []byte

	if strings.HasPrefix(criteria.JSON, "@") {
		filename := criteria.JSON[1:]
		var err error
		content, err = ioutil.ReadFile(filename)
		if err != nil {
			return nil, []error{fmt.Errorf("Failed to read input file: %s", filename), err}
		}
	} else {
		content = []byte(criteria.JSON)
	}
	return content, nil
}

func (pinata Pinata) buildPinJSONRequest(content []byte, criteria PinJSONCriteria) ([]byte, []error) {

	unionCriteria := pinCriteriaUnion{
		CIDVersion:       criteria.CIDVersion,
		Name:             criteria.Name,
		KeyValues:        criteria.KeyValues,
		ReplicationInfos: criteria.ReplicationInfos,
	}

	var contentJSON interface{}
	var requestBody []byte

	err := json.Unmarshal(content, &contentJSON)
	if err != nil {
		return nil, []error{fmt.Errorf("Failed to parse content into JSON"), err}
	}

	request := pinJSONRequest{
		PinataOptions:  pinata.buildPinataOptions(unionCriteria),
		PinataMetadata: pinata.buildPinataMetadata(unionCriteria),
		PinataContent:  &contentJSON,
	}

	requestBody, err = json.Marshal(request)
	if err != nil {
		return nil, []error{fmt.Errorf("Failed to marshal criteria to JSON"), err}
	}

	return requestBody, nil
}

func (pinata Pinata) hasPinataContent(jsonp []byte) (bool, []error) {

	var array []interface{}
	json.Unmarshal(jsonp, &array)
	if len(array) > 0 {
		return false, nil
	}

	var simple pinJSONRequest
	err := json.Unmarshal(jsonp, &simple)
	if err != nil {
		return false, []error{fmt.Errorf("Failed to parse JSON looking for pinataContent"), err}
	}
	return simple.PinataContent != nil, nil
}

func (pinata Pinata) pinJSON(ids PinataIdentifiers, criteria PinJSONCriteria) (PinJSONResult, []error) {

	var result PinJSONResult

	var requestBody []byte

	content, errs := pinata.getJSONContent(criteria)
	if len(errs) > 0 {
		return result, errs
	}

	if criteria.CIDVersion == "" && criteria.Name == "" && len(criteria.KeyValues) == 0 && len(criteria.ReplicationInfos) == 0 {
		log.Trace("Simply copy JSON as content")
		requestBody = content
	} else {
		hasPinataContent, errs := pinata.hasPinataContent(content)
		if len(errs) > 0 {
			return result, errs
		}
		if hasPinataContent {
			log.Warn("Some flags will be ignored since JSON is already containing options and metadata")
			requestBody = content
		} else {
			log.Trace("Let's build the whole request")
			requestBody, errs = pinata.buildPinJSONRequest(content, criteria)
			if len(errs) > 0 {
				return result, errs
			}
		}
	}

	query := "https://api.pinata.cloud/pinning/pinJSONToIPFS"
	log.Debug("POST ", query)
	log.Trace("Request body: ", string(requestBody))

	req, err := http.NewRequest("POST", query, bytes.NewBuffer(requestBody))
	if err != nil {
		return result, []error{fmt.Errorf("Failed to prepare request for Pinata server with POST at %s", query), err}
	}

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("pinata_api_key", ids.ApiKey)
	req.Header.Add("pinata_secret_api_key", ids.SecretApiKey)

	resp, err := httpClient.Do(req)

	if err != nil {
		return result, []error{fmt.Errorf("Failed to call Pinata server with POST at %s", query), err}
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return result, []error{fmt.Errorf("Failed to read body while calling Pinata server with POST at %s", query), err}
	}

	log.Trace("Status code: ", resp.Status)
	log.Trace("Response body: ", string(body))

	if resp.StatusCode != 200 {
		return result, []error{fmt.Errorf("Error while calling Pinata server with POST at %s", query),
			fmt.Errorf("Status code: %s", resp.Status),
			fmt.Errorf("Response body: %s", string(body)),
		}
	}

	var response pinJSONResult
	err = json.Unmarshal(body, &response)
	if err != nil {
		return result, []error{fmt.Errorf("Failed to parse JSON response body while calling Pinata server with POST at %s", query), err}
	}

	log.Trace("Response unmarshalled: ", response.String())

	result = PinJSONResult{
		IpfsHash:  response.IpfsHash,
		PinSize:   response.PinSize,
		Timestamp: response.Timestamp,
	}

	log.Trace("Return object: ", result.String())

	return result, nil
}
