package pinatacli

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	log "github.com/sirupsen/logrus"
)

type listPinJobsResult struct {
	Count int `json:"count"`
	Rows  []struct {
		ID          string    `json:"id"`
		IpfsPinHash string    `json:"ipfs_pin_hash"`
		DateQueued  time.Time `json:"date_queued"`
		Name        string    `json:"name"`
		Status      string    `json:"status"`
	} `json:"rows"`
}

type ListPinJobsCriteria struct {
	IpfsPinHash string
	Sort        string
	Status      string
}

func (pinata Pinata) getPinJobsList(ids PinataIdentifiers, criteria ListPinJobsCriteria) ([]PinataPinJob, []error) {

	iPage := 0
	var pinJobs []PinataPinJob

	for {

		query := fmt.Sprintf("https://api.pinata.cloud/pinning/pinJobs?limit=%d&offset=%d", pageSize, iPage*pageSize)
		if len(criteria.IpfsPinHash) > 0 {
			query += "&ipfs_pin_hash=" + criteria.IpfsPinHash
		}
		if len(criteria.Sort) > 0 {
			query += "&sort=" + criteria.Sort
		}
		if len(criteria.Status) > 0 {
			query += "&status=" + criteria.Status
		}
		log.Debug("GET ", query)

		req, err := http.NewRequest("GET", query, nil)
		if err != nil {
			return nil, []error{fmt.Errorf("Failed to prepare request for Pinata server with GET at %s", query), err}
		}

		req.Header.Add("pinata_api_key", ids.ApiKey)
		req.Header.Add("pinata_secret_api_key", ids.SecretApiKey)

		resp, err := httpClient.Do(req)

		if err != nil {
			return nil, []error{fmt.Errorf("Failed to call Pinata server with GET at %s", query), err}
		}
		defer resp.Body.Close()
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return nil, []error{fmt.Errorf("Failed to read body while calling Pinata server with GET at %s", query), err}
		}
	
		log.Trace("Status code: ", resp.Status)
		log.Trace("Response body: ", string(body))
		
		if resp.StatusCode != 200 {
			return nil, []error{fmt.Errorf("Error while calling Pinata server with GET at %s", query),
				fmt.Errorf("Status code: %s", resp.Status),
			}
		}
		
		var response listPinJobsResult
		err = json.Unmarshal(body, &response)
		if err != nil {
			return nil, []error{fmt.Errorf("Failed to parse JSON response body while calling Pinata server with GET at %s", query), err}
		}

		if len(response.Rows) == 0 {
			log.Debug("No more items found")
			break
		}

		for _, pinJob := range response.Rows {
			pinJobs = append(pinJobs, PinataPinJob{
				ID:          pinJob.ID,
				IpfsPinHash: pinJob.IpfsPinHash,
				DateQueued:  pinJob.DateQueued,
				Name:        pinJob.Name,
				Status:      pinJob.Status,
			})
			log.Trace(pinJobs[len(pinJobs)-1])
		}

		log.Debugf("%d items in list, %d expected", len(pinJobs), response.Count)

		if len(pinJobs) == response.Count {
			log.Debug("All items found")
			break
		}

		iPage++

	}

	return pinJobs, nil
}
