package pinatacli

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	log "github.com/sirupsen/logrus"
)

type userPinnedDataTotalResult struct {
	PinCount                     int    `json:"pin_count"`
	PinSizeTotal                 string `json:"pin_size_total"`
	PinSizeWithReplicationsTotal string `json:"pin_size_with_replications_total"`
}

type UserPinnedDataTotalResult struct {
	PinCount                     int `json:"pin_count"`
	PinSizeTotal                 int `json:"pin_size_total"`
	PinSizeWithReplicationsTotal int `json:"pin_size_with_replications_total"`
}

func (result userPinnedDataTotalResult) String() string {
	return fmt.Sprintf("PinCount: %d | PinSizeTotal: %s | PinSizeWithReplicationsTotal: %s",
		result.PinCount, result.PinSizeTotal, result.PinSizeWithReplicationsTotal)
}

func (result UserPinnedDataTotalResult) String() string {
	return fmt.Sprintf("PinCount: %d | PinSizeTotal: %d | PinSizeWithReplicationsTotal: %d",
		result.PinCount, result.PinSizeTotal, result.PinSizeWithReplicationsTotal)
}

func (pinata Pinata) getUserPinnedDataTotal(ids PinataIdentifiers) (UserPinnedDataTotalResult, []error) {

	var result UserPinnedDataTotalResult

	query := "https://api.pinata.cloud/data/userPinnedDataTotal"
	log.Debug("GET ", query)

	req, err := http.NewRequest("GET", query, nil)
	if err != nil {
		return result, []error{fmt.Errorf("Failed to prepare request for Pinata server with POST at %s", query), err}
	}

	req.Header.Add("pinata_api_key", ids.ApiKey)
	req.Header.Add("pinata_secret_api_key", ids.SecretApiKey)

	resp, err := httpClient.Do(req)

	if err != nil {
		return result, []error{fmt.Errorf("Failed to call Pinata server with GET at %s", query), err}
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return result, []error{fmt.Errorf("Failed to read body while calling Pinata server with GET at %s", query), err}
	}

	log.Trace("Status code: ", resp.Status)
	log.Trace("Response body: ", string(body))
		
	if resp.StatusCode != 200 {
		return result, []error{fmt.Errorf("Error while calling Pinata server with GET at %s", query),
			fmt.Errorf("Status code: %s", resp.Status),
		}
	}

	var response userPinnedDataTotalResult
	err = json.Unmarshal(body, &response)
	if err != nil {
		return result, []error{fmt.Errorf("Failed to parse JSON response body while calling Pinata server with GET at %s", query), err}
	}

	log.Trace("Response unmarshalled: ", response.String())

	pinSizeTotal, err := strconv.Atoi(response.PinSizeTotal)
	if err != nil {
		return result, []error{fmt.Errorf("Failed to parse int value of pinSizeTotal"), err}
	}

	pinSizeWithReplicationsTotal, err := strconv.Atoi(response.PinSizeWithReplicationsTotal)
	if err != nil {
		return result, []error{fmt.Errorf("Failed to parse int value of pinSizeWithReplicationsTotal"), err}
	}

	result = UserPinnedDataTotalResult{
		PinCount:                     response.PinCount,
		PinSizeTotal:                 pinSizeTotal,
		PinSizeWithReplicationsTotal: pinSizeWithReplicationsTotal,
	}

	log.Trace("Return object: ", result.String())

	return result, nil
}
