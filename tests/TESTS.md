# Tests

## Automated unit tests

Some unit tests are automated. Since this CLI is mostly integration only, there is not much business logic to test and hence the number of automated tests are quite limited.

## Manual integration tests

Most tests are still ran manually, in integration with the real Pinata systems.

Below is a description of the test scenario ran manually before each release.

- set correct credentials in profile default of ~/.pinata/credentials

- pin some JSON object
```
./pinata pin --json '{"key":"value"}'
```

- unpin it
```
./pinata unpin --hash QmfMfXquxZeAEGwRGYua5DB1n5FBSs3xTaovXLG4ygZ9Bc
```

- pin some JSON array
```
./pinata pin --json '[1, 2, 3]'
```

- unpin it
```
./pinata unpin --hash QmSPHAJRx5pN1iDJhudCnwWvsPGEfBD2KothrjiX6MJBjA
```

- pin a JSON object
```
./pinata pin --json @../tests/object.json
```

- unpin it
```
./pinata unpin --hash QmfMfXquxZeAEGwRGYua5DB1n5FBSs3xTaovXLG4ygZ9Bc
```

- pin a JSON object with a name
```
./pinata pin --json @../tests/object.json --name "bob"
```

- check its name
```
./pinata list-pins --hash QmfMfXquxZeAEGwRGYua5DB1n5FBSs3xTaovXLG4ygZ9Bc --status pinned
```

- unpin it
```
./pinata unpin --hash QmfMfXquxZeAEGwRGYua5DB1n5FBSs3xTaovXLG4ygZ9Bc
```

- pin a JSON array
```
./pinata pin --json @../tests/array.json
```

- unpin it
```
./pinata unpin --hash QmSPHAJRx5pN1iDJhudCnwWvsPGEfBD2KothrjiX6MJBjA
```

- pin a JSON array with CID v1
```
./pinata pin --json @../tests/array.json --cidversion 1
```

- unpin it
```
./pinata unpin --hash bafkreifgcxxk5yq54ulz3yea32gdauwi3kibcocanotryogagkcf67ku6q
```

- pin a JSON array with metadata in the file
```
./pinata pin --json @../tests/arrayWithMetadata.json
```

- check its name
```
./pinata list-pins --hash QmSPHAJRx5pN1iDJhudCnwWvsPGEfBD2KothrjiX6MJBjA --status pinned
```

- unpin it
```
./pinata unpin --hash QmSPHAJRx5pN1iDJhudCnwWvsPGEfBD2KothrjiX6MJBjA
```

- pin a JSON object with metadata in the file
```
./pinata pin --json @../tests/objectWithMetadata.json
```

- check its name
```
./pinata list-pins --hash QmfMfXquxZeAEGwRGYua5DB1n5FBSs3xTaovXLG4ygZ9Bc --status pinned
```

- unpin it
```
./pinata unpin --hash QmfMfXquxZeAEGwRGYua5DB1n5FBSs3xTaovXLG4ygZ9Bc
```

- pin a JSON object with metadata in the file and try to reset its name
```
./pinata pin --json @../tests/objectWithMetadata.json --name "another_name"
```
A warning should be displayed

- check its name (should be "a_name")
```
./pinata list-pins --hash QmfMfXquxZeAEGwRGYua5DB1n5FBSs3xTaovXLG4ygZ9Bc --status pinned
```

- unpin it
```
./pinata unpin --hash QmfMfXquxZeAEGwRGYua5DB1n5FBSs3xTaovXLG4ygZ9Bc
```

- pin a single file
```
./pinata pin --file ../tests/test_folder/array.json
```

- unpin it
```
./pinata unpin --hash Qma4ySZrfEBUn7U2m6zV1mQKrvfwm4CdX7H2E2nfbS7zsk
```

- pin a directory without wrapping
```
./pinata pin --file ../tests/test_folder
```

- unpin it
```
./pinata unpin --hash QmP1DoLDN2CbdqDNN1XavhGgpyBdh9nAxjnWH1FTk5oNAP
```

- pin a directory with wrapping
```
./pinata pin --file ../tests/test_folder -w
```

- unpin it
```
./pinata unpin --hash QmVS6vn76MoENHnsGm6VSrAgLwfXQit7phmqRvSgAa2YiY
```

- pin a hash on an IPFS node (not managed by Pinata, any web accessible node is OK)

- start of watch on pin jobs lists (sort descending)
```
watch -n 1 "./pinata list-pin-jobs --sort DESC | jq '.Items[].IpfsPinHash'"
```
- pin this hash on Pinata, with a host to indicate the node location, a name and a custom keyvalue

```
./pinata pin --host /ip4/hostNode1ExternalIP/tcp/4001/ipfs/hostNode1PeerId --hash Qmati31SFEgSQbziiVs6bVk1r7fSXfUtgVxkyrDULXGSSM --name a_super_name --keyvalue key1:value1
```

- check that the job appeared
- check that the job succeeded
- search for the failing job by hash (the failing job can be recreated with a fake hash value at any time) and by a non-matching hash

```
./pinata list-pin-jobs --hash QmdYTBNig2d4dQd5o1LXM3NHbCYA7168NpN5R9m44vDjL8
```

- search for the failing job by status and by a non-matching status

```
./pinata list-pin-jobs --status searching
```

- search for the pin by hash

```
./pinata list-pins --hash Qmati31SFEgSQbziiVs6bVk1r7fSXfUtgVxkyrDULXGSSM | jq ".Count"
```

- search for the pin by name

```
./pinata list-pins --name a_super_name | jq ".Count"
```

- search for the pin by metadata key value

```
./pinata list-pins --keyvaluesquery '{"key1":{"value":"%al%","op":"like"}}' | jq ".Count"
```

-  search for the pin by status

```
./pinata list-pins --status pinned | jq ".Count"
```

-  search for the pin by pin_end

```
./pinata list-pins --pin-end "2020-03-21T18:00:00Z" | jq ".Count"
```

-  search for the pin by pin_start

```
./pinata list-pins --pin_start "2020-03-21T18:00:00Z" | jq ".Count"
```

- sum of the two above must be equal to the grand total without criteria


-  search for the pin by unpin_end

```
./pinata list-pins --unpin-end "2020-03-21T18:00:00Z" | jq ".Count"
```

-  search for the pin by unpin_start

```
./pinata list-pins --unpin_start "2020-03-21T18:00:00Z" | jq ".Count"
```

- sum of the two above must be equal to the grand total without criteria + the number of hash still pinned (they appear in both result set)


-  search for the pin by size_min

```
./pinata list-pins --size-min 3000000 | jq ".Count"
```

-  search for the pin by size_max

```
./pinata list-pins --size-max 3000000 | jq ".Count"
```

- unpin this hash from Pinata

```
./pinata unpin --hash Qmati31SFEgSQbziiVs6bVk1r7fSXfUtgVxkyrDULXGSSM
```

- pin this hash again on Pinata, with different replication than your default one

```
./pinata pin  --hash Qmati31SFEgSQbziiVs6bVk1r7fSXfUtgVxkyrDULXGSSM --region NYC1:1
```

- check it has been replicated on the right region

```
./pinata list-pins  --hash Qmati31SFEgSQbziiVs6bVk1r7fSXfUtgVxkyrDULXGSSM --status pinned
```

- unpin this hash definitely

```
./pinata unpin --hash Qmati31SFEgSQbziiVs6bVk1r7fSXfUtgVxkyrDULXGSSM
```

- check the global user info

```
./pinata info --key $PINATA_API_KEY --secret $PINATA_SECRET_API_KEY
```

- change your default config

```
./pinata config --region "NYC1:1"
```

- check it in the UI

- change your config back to the original

```
./pinata config --region "FRA1:1"
```

- change to wrong_type profile and check that a proper message is displayed

```
./pinata info --profile wrong_type
```

- change to wrong_values profile and check that a proper message is displayed

```
./pinata info --profile wrong_values
```

- set correct credentials values in environment variables.

```
export PINATA_API_KEY=correct_api_key
 export PINATA_SECRET_API_KEY=correct_secret
./pinata info --profile wrong_values
```

- set incorrect credentials values in environment variables  and check that a proper message is displayed

```
export PINATA_API_KEY=wrong_api_key
 export PINATA_SECRET_API_KEY=wrong_secret
./pinata info --profile wrong_values
```

- set correct credentials values in command line.

```
./pinata info --key "correct_key" --secret "correct_secret" --profile wrong_values
```

- set incorrect credentials values in environment variables and check that a proper message is displayed

```
./pinata info --key "incorrect_key" --secret "incorrect_secret" --profile wrong_values
```